package org.bitbucket.cggaertner.lib;

import java.io.InputStream;
import java.io.IOException;
import java.net.URI;
import java.net.http.HttpClient;
import java.net.http.HttpHeaders;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;
import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;
import java.time.Duration;
import java.util.List;
import java.util.Optional;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.CompletionException;
import java.util.stream.Stream;
import java.util.zip.GZIPInputStream;
import javax.net.ssl.SSLContext;

public class http {
    public static final String SSL_PROTOCOL = "TLSv1.3";
    public static final Duration DEFAULT_CONNECT_TIMEOUT = Duration.ofSeconds(15);
    public static final Duration DEFAULT_REQUEST_TIMEOUT = Duration.ofSeconds(15);

    public static class Client {
        private HttpClient client;

        private Client(HttpClient client) {
            this.client = client;
        }

        public HttpClient unbox() {
            return client;
        }

        public AsyncResponse get(String uri) {
            return get(URI.create(uri), DEFAULT_CONNECT_TIMEOUT);
        }

        public AsyncResponse get(String uri, Duration timeout) {
            return get(URI.create(uri), timeout);
        }

        public AsyncResponse get(URI uri) {
            return get(uri, DEFAULT_CONNECT_TIMEOUT);
        }

        public AsyncResponse get(URI uri, Duration timeout) {
            HttpRequest req = HttpRequest.newBuilder(uri)
                .timeout(timeout)
                .header("Accept-Encoding", "gzip")
                .GET()
                .build();

            return new AsyncResponse(uri, client.sendAsync(req,
                HttpResponse.BodyHandlers.ofInputStream()));
        }
    }

    public static Client client() {
        return client(DEFAULT_CONNECT_TIMEOUT);
    }

    public static Client client(Duration timeout) {
        try {
            SSLContext context = SSLContext.getInstance(SSL_PROTOCOL);
            context.init(null, null, null);

            return new Client(HttpClient.newBuilder()
                .sslContext(context)
                .connectTimeout(timeout)
                .followRedirects(HttpClient.Redirect.NORMAL)
                .build());
        }
        catch(NoSuchAlgorithmException | KeyManagementException e) {
            throw new RuntimeException(e);
        }
    }

    public static class AsyncResponse {
        private static final Optional GZIP = Optional.of("gzip");

        public final URI requestUri;
        public final CompletableFuture<HttpResponse<InputStream>> future;

        public AsyncResponse(URI uri,
                CompletableFuture<HttpResponse<InputStream>> future) {
            this.requestUri = uri;
            this.future = future;
        }

        public Response await() {
            try  {
                var response = future.join();
                InputStream body = response.body();

                var enc = response.headers().firstValue("Content-Encoding");
                if(enc.equals(GZIP)) try {
                    body = new GZIPInputStream(body);
                }
                catch(IOException e) {
                    try {
                        body.close();
                    }
                    catch(IOException dummy) {}
                    throw new CompletionException(e);
                }
                
                return new Response(response.statusCode(), requestUri,
                    response.uri(), response.headers(), body);
            }
            catch(CompletionException e) {
                return new Response(requestUri, e.getCause());
            }
        }
    }

    public static class Response {
        public final int status;
        public final URI requestUri;
        public final URI responseUri;
        public final HttpHeaders headers;
        public final InputStream body;
        public final Throwable failure;

        private Response(int status, URI requestUri, URI responseUri,
                HttpHeaders headers, InputStream body) {
            this.status = status;
            this.requestUri = requestUri;
            this.responseUri = responseUri;
            this.headers = headers;
            this.body = body;
            this.failure = null;
        }

        private Response(URI requestUri, Throwable failure) {
            this.requestUri = requestUri;
            this.failure = failure;
            this.status = 0;
            this.responseUri = null;
            this.headers = null;
            this.body = null;
        }

        public boolean failed() {
            return status == 0;
        }

        public Optional<String> header(String name) {
            return headers.firstValue(name);
        }

        public List<String> headers(String name) {
            return headers.allValues(name);
        }
    }
}
