package org.bitbucket.cggaertner.lib;

import java.io.PrintStream;

public class stdio {
    public static void println(PrintStream printer, Object... objects) {
        for(var obj : objects) printer.print(obj.toString());
        printer.println();
    }

    public static void say(Object... objects) {
        println(System.out, objects);
    }

    public static void cry(Object... objects) {
        println(System.err, objects);
    }
}
