package org.bitbucket.cggaertner.lib;

import java.nio.file.Path;
import java.nio.file.Paths;

public class system {
    private static final Path USER_HOME;

    static {
        USER_HOME = Paths.get(System.getProperty("user.home"));
    }

    public static void exit() {
        System.exit(0);
    }

    public static void exit(int code) {
        System.exit(code);
    }

    public static void bye() {
        System.exit(0);
    }

    public static void bye(Object... msg) {
        stdio.say(msg);
        System.exit(0);
    }

    public static void die() {
        System.exit(1);
    }

    public static void die(Object... msg) {
        stdio.cry(msg);
        System.exit(1);
    }

    public static Path homepath(String child) {
        return USER_HOME.resolve(child);
    }
}
