package org.bitbucket.cggaertner.lib;

public class objects {
    public static boolean eq(Object a, Object b) {
        return a == null ? b == null : a.equals(b);
    }

    public static boolean any(Object... objects) {
        for(var obj : objects) {
            if(obj != null)
                return true;
        }

        return false;
    }

    public static String classname(Object obj) {
        return obj.getClass().getSimpleName();
    }
}
