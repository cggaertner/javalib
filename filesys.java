package org.bitbucket.cggaertner.lib;

import java.io.IOException;
import java.nio.channels.FileChannel;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.StandardOpenOption;
import java.nio.file.attribute.FileAttribute;
import java.nio.file.attribute.PosixFilePermission;
import java.nio.file.attribute.PosixFilePermissions;
import java.util.HashSet;
import java.util.Set;

public class filesys {
    public static FileAttribute<Set<PosixFilePermission>>
            permissions(int value) {

        var set = new HashSet<PosixFilePermission>();

        if((value & 0400) != 0) set.add(PosixFilePermission.OWNER_READ);
        if((value & 0200) != 0) set.add(PosixFilePermission.OWNER_WRITE);
        if((value & 0100) != 0) set.add(PosixFilePermission.OWNER_EXECUTE);

        if((value & 0040) != 0) set.add(PosixFilePermission.GROUP_READ);
        if((value & 0020) != 0) set.add(PosixFilePermission.GROUP_WRITE);
        if((value & 0010) != 0) set.add(PosixFilePermission.GROUP_EXECUTE);

        if((value & 0004) != 0) set.add(PosixFilePermission.OTHERS_READ);
        if((value & 0002) != 0) set.add(PosixFilePermission.OTHERS_WRITE);
        if((value & 0001) != 0) set.add(PosixFilePermission.OTHERS_EXECUTE);

        return PosixFilePermissions.asFileAttribute(set);
    }

    public static void createfile(Path path) throws IOException {
        Files.createFile(path);
    }

    public static void createfile(Path path, int perm) throws IOException {
        Files.createFile(path, permissions(perm));
    }

    public static void makedir(Path path) throws IOException {
        Files.createDirectory(path);
    }

    public static void makedir(Path path, int perm) throws IOException {
        Files.createDirectory(path, permissions(perm));
    }

    public static void makepath(Path path) throws IOException {
        Files.createDirectories(path);
    }

    public static void makepath(Path path, int perm) throws IOException {
        Files.createDirectories(path, permissions(perm));
    }

    public static void truncate(Path path, long size) throws IOException {
        try(var fc = FileChannel.open(path, StandardOpenOption.WRITE)) {
            fc.truncate(size);
        }
    }
}
